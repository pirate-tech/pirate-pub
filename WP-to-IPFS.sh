# KNOWN ISSUES
# 

# PREPARE .SH SHELL FILE FOR USE
# Put this file in path/FreeMansPerspective/Subdir (path/FreeMansPerspective can be anything but Subdir must have that name)
# Right click on this file in File Manager
# Select Properties in pop-up menu
# Select Permissions tab
# Check box for "Allow this file to run as a program"
# Select Close

# INSTALL WGET AND FIND COMMANDS, IF NEEDED
# It may be necessary to install a couple of Linux commands, wget and find, 
#  which are necessary for this program to run. 
# You can test to see if they're already installed with the following to commands:
#   wget --help
#   find --help
# If either is not recognized, you can install with the following commands, respectively:
#   sudo apt install wget
#   sudo apt install find

# GENERATE IPNS PUBLISHING KEY - ONE TIME ONLY
# ipfs key gen --type=rsa --size=2048 "FreeMansPerspective.com"
# Copy and save the IPNS key hash generated
#   everywhere below replace k2k... with key hash generated
#   Website will be at
#   https://ipfs.io/ipns/k2k4r8opjzlv6tcsmp80bigim0tty4f9mdm1y19ijjmqddumof12o3ys/freemansperspective.com/
#   or
#   ipns://k2k4r8opjzlv6tcsmp80bigim0tty4f9mdm1y19ijjmqddumof12o3ys/freemansperspective.com/
IPNS_hash=k2k4r8opjzlv6tcsmp80bigim0tty4f9mdm1y19ijjmqddumof12o3ys

# CHANGING WEBSITE SOURCE LOCATION
# If the website source location is changed at the end of the lines beginning with wget, 
# After the website import begins, verify the expected directory name has been created.

# VERIFY IPFS DESKTOP IS OPERATING
# There should be an IPFS icon indicating it's operating.
# Right-click on the icon and open the status window to see how many peers are connected.
# If no peers are indicated, check the internet connection.
# Restart the IPFS node if it's been on for a long time and is misbehaving.

# EXECUTE THIS .SH SHELL FILE
#   Open a CLI (command line interface) terminal window in /Subdir containing FreeMansPerspective.sh
#   Enter ./FreeMansPerspective.sh

# Delete old html files to all be replaced
find . -type f -name "*.html" -delete

# DOWNLOAD WEBSITE WITH INTERNAL CONTENT ONLY
# NOTE: It's much better if website url can be replaced with a mapped network directory or server IP address
#   for faster downloading and much more assured access to the source files.
# Include directories referenced as 'subdomain.FreeMansPerspective.com' with domain limited span
# Directories created under /Subdir are FreeMansPerspective.com and traffic.libsyn.com where the media is located. -e robots=off is necessary to innore robot exclusion, so the media files will import
wget –user-agent="Mozilla" -e robots=off --convert-links --recursive --level=inf --page-requisites --adjust-extension --timestamping --restrict-file-names=windows --no-parent -H -Dfreemansperspective.com,libsyn.com https://freemansperspective.com/

# FIND/REPLACE LINKS
echo "find and replace http:// with https://"
find . -type f -name "*.html" -exec sed -i 's|http://|https://|g' {} \;

echo "find and replace https://freemansperspective.com with /freemansperspective.com"
find . -type f -name "*.html" -exec sed -i 's|https://freemansperspective.com|/freemansperspective.com|g' {} \;

echo "find and replace https://traffic.libsyn.com with /traffic.libsyn.com"
find . -type f -name "*.html" -exec sed -i 's|https://traffic.libsyn.com|/traffic.libsyn.com|g' {} \;

# ADD WEBSITE DIRECTORY TO IPFS
# and save new IPFS hash of directory and copy to log file
echo "There will be a very long pause while adding to IPFS node"
cd ..
IPFS_hash=$(ipfs add -r -Q Subdir)
cd Subdir
echo $IPFS_hash >> ipfs_hash_log.txt

# PUBLISH TO IPNS
echo "publishing $IPFS_hash to IPNS"
ipfs name publish --key=$IPNS_hash $IPFS_hash
echo "Publishing is complete"
echo "enter 'exit' to close window"
